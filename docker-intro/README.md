# an introduction to Docker
 (General impression is that it is a powerful tool, should be used with caution)

## Intro
Once had a developer ask me if Docker was magic. Docker is not magic, but it is magical.

What do you mean when you say Docker?

Docker is a broad subject:
1. Are you talking Docker the Company?
2. Are you talking Docker the CLI tool?
3. Are you talking Docker the container?
4. Are you talking Docker the daemon?

## Quick Demo

```sh
source ../prompt.sh
```

Step 1. getting docker installed and setup:

``` sh
firefox https://docs.docker.com/install/
firefox https://hub.docker.com/editions/community/docker-ce-desktop-windows
firefox https://hub.docker.com/editions/community/docker-ce-desktop-mac
snap list docker
```

Step 2. docker pull,  docker images, Docker run, docker ps, docker exec, docker rm

``` sh
docker ps -a
docker pull mcr.microsoft.com/dotnet/core/samples:aspnetapp
docker images
firefox http://localhost:8000 
docker run -it -p 8000:80 --name aspnetcore_sample mcr.microsoft.com/dotnet/core/samples:aspnetapp
docker ps -a
```

``` sh
docker rm aspnetcore_sample
docker rmi mcr.microsoft.com/dotnet/core/samples:aspnetapp
docker run -d -p 8000:80 --name aspnetcore_sample mcr.microsoft.com/dotnet/core/samples:aspnetapp
docker logs aspnetcore_sample
docker exec -it aspnetcore_sample /bin/bash

# inside the container
ls
ls wwwroot
cat /etc/os-release
exit

docker rm aspnetcore_sample
docker stop aspnetcore_sample
docker rm aspnetcore_sample
```

step 3. Dockerfile, docker layers. docker build
https://github.com/dotnet/dotnet-docker/tree/master/samples/aspnetapp

```
cd dotnet-docker/samples/aspnetapp
docker build -f Dockerfile.dev -t example
docker run -d -p 8000:80 --volume .:/app --read-only --name app1 example
vim aspnetapp/Views/Home/Index.cshtml
firefox localhost:8000
docker logs app1
```

# for digging into the details
docker inspect app1 | less
docker inspect example | less

## Some theory.
Each of these is important for different reasons:
Docker the company is important from a business point of view
  - Which edition are you using?
  - What kind of support are you looking for?
  -
Docker the CLI tool is important for developers/QA
  - different command line parameters, hooking up to different Docker Serrvers with the client
Docker the container is important for security
Docker the daemon is important for operations



