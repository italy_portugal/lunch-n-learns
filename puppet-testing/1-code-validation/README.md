# Validating Puppet Code

#### Table of Contents
1. [Commands](#commands)
1. [Code](#code)
1. [Notes](#notes)

## Commands

``` sh
bundle install; bundle exec rake

echo 'gem "puppet", "~> 3.5"' >> Gemfile; \
echo "gem 'safe_yaml'" >> Gemfile; \
vim Rakefile

mkdir manifests; \
touch manifests/init.pp; \
vim manifests/init.pp

bundle update; bundle exec rake; \
bundle exec rake syntax

echo "gem 'puppet-syntax'" >> Gemfile; \
vim Rakefile

bundle update; bundle exec rake; \
bundle exec rake syntax
```

## Code

Rakefile (V1)
``` ruby
task :default do
  system('rake -T')
end

desc 'run puppet parser validate over all files'
task :syntax do
  sh 'find -name "*.pp" | xargs puppet parser validate' do |ok, _res|
    puts 'validation check passed' if ok
  end
end
```

manifests/init.pp
``` puppet
# Just an Example Class to test
class 'test' {
    file {'/root/test.txt':
        ensure => 'presnet',
        content => 'Hello World',
    }
}
```

Rakefile (V2)
``` ruby
require 'puppet-syntax/tasks/puppet-syntax'

task :default do
  system('rake -T')
end
```

## Notes
Goal: Validate the syntax of puppet code

We are going to modify the environment for validating puppet
code. This gives the same level of confidence as the puppet
smoker.

Check that we are starting from the same point.

Add the puppet gem to the Gemfile, we want to make sure that
we have control over the version of puppet (this ensures that
we have the latest in the 3.x series after puppet 3.5) add the
safe_yaml gem based on our previous experience.

add the validate rake task and modify the default rake task
to list the defined tasks

create a basic puppet class

fix the validation error (quotes around the class name)

we can do even better, thanks to a module from the puppet community:
puppet-syntax gem brings with it also template and hiera validation as well.
