# Linting Puppet Code

#### Table of Contents
1. [Commands](#commands)
1. [Code](#code)
1. [Notes](#notes)
1. [Links](#links)

## Commands

```sh
bundle install; bundle exec rake; \
bundle exec rake syntax

echo "gem 'puppet-lint'" >> Gemfile; \
vim Rakefile

bundle install; bundle exec rake; \
bundle exec rake lint
```

## Code

Rakefile
``` ruby
require 'puppet-lint/tasks/puppet-lint'

PuppetLint::RakeTask.new :lint do |config|
    config.disable_checks = ['80chars', 'autoloader_layout']
    config.fail_on_warnings = true
    config.pattern = '**/*.pp'
end
```

## Notes
Goal: Linting code according to Puppet Style.

We are going to now setup the puppet-lint tool; useful for determining 
if the code follows the community puppet style guide.

Check that we are starting from the same point.

Add the puppet-lint gem to the Gemfile

add the lint rake task (comment on disabled checks.)

fix the linting warning (inconsistent arrows)

reminder: we can now increase consistency of our style across 
our codebase.

side note (if time): puppet-lint has the ability to enforce the style in most cases.

## Links

- [Puppet language Style Guide](https://docs.puppet.com/puppet/latest/style_guide.html)
- [puppet-lint website](http://puppet-lint.com/)
- [puppet-lint repo](https://github.com/rodjek/puppet-lint)
