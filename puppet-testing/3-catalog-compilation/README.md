# Compiling Puppet Code

#### Table of Contents
1. [Commands](#commands)
1. [Code](#code)
1. [Notes](#notes)
1. [Links](#links)

## Commands
``` sh
bundle install; bundle exec rake; \
bundle exec rake syntax; \
bundle exec rake lint

echo "gem 'puppetlabs_spec_helper'" >> Gemfile; \
vim Rakefile

bundle update; bundle exec rake

mkdir spec; \
touch spec/spec_helper.rb; \
echo "require 'puppetlabs_spec_helper/module_spec_helper'" >> spec/spec_helper.rb

mkdir spec/classes; \
touch spec/classes/test_spec.rb; \
vim spec/classes/test_spec.rb

touch .fixtures.yml; \
vim .fixtures.yml

bundle exec rake spec

vim spec/classes/test_spec.rb; \
bundle exec rake spec

vim spec/classes/test_spec.rb; \
bundle exec rake spec

vim manifests/init.pp; \
bundle exec rake spec
```
## Code

Rakefile
``` ruby
require 'puppetlabs_spec_helper/rake_tasks'

# Remove Everything except the lint task
```

.fixtures.yml
``` yaml
fixtures:
  symlinks:
    test: "#{source_dir}"
```

spec/classes/test_spec.rb (V1)
``` ruby
require 'spec_helper'

describe 'test' do
    it { is_expected.to compile }
end
```

spec/classes/test_spec.rb (V2)
``` ruby
require 'spec_helper'

describe 'test' do
    it { is_expected.to compile }
    it { is_expected.to contain_file('/root/test.txt').with(
        'ensure' => 'present',
        'content' => /World/
        ) }
end
```

spec/classes/test_spec.rb (V3)
``` ruby
require 'spec_helper'

describe 'test' do
    it { is_expected.to compile }
    it { is_expected.to contain_file('/root/test.txt').with(
        'ensure' => 'present',
        'content' => /World/
        ) }
    context 'on a node running CentOS' do
        let(:facts) do
            { os: 'CentOS' }
        end
        it { is_expected.to contain_notify('on CentOS') }
    end
    context 'on a node running Debian' do
        let(:facts) do
            { os: 'Debian' }
        end
        it { is_expected.to contain_notify('on Debian') }
    end
end
```
manifests/init.pp
``` ruby
## == Class: test
# Just an Example Class to test
class test {
    file { '/root/test.txt':
        ensure  => 'present',
        content => 'Hello World',
    }

    notify { "on ${::os}": }
}
```

## Notes
Goal: Testing results of catalog compilation

Now, we are going to test the results of catalog compilation;
useful for checking compile-time behavior

Check that we are starting from the same point.

The puppet_labs_spec_helper gem leverages Ruby's Rspec Testing
tool and the library puppet-rspec for testing catalog compilation.

puppetlabs_spec_helper introduces several new rake tasks and
includes the other rake tasks that we've manually created thus
far. I'm not removing the lint task because it's customized.

spec_helper.rb is use to prevent unnecessary repetition of setup
code at the beginning of every test file

.fixtures.yml tracks the puppet module's dependencies, which,
due to puppetlabs_spec_helper, must include the module itself.
(if questioned about duplication with metadata.json, mention
there are pull requests to remove the duplication)

The simplest test is whether the catalog compiles. (yes you
can test for catalog compilation that errors). by convention
all ruby rspec test files are suffixed with `_spec.rb`

The next simplest test is whether a specific resource exists.

You can also test the parameters passed to a resource

When testing conditionals or variable interpolation, you can
setup different contexts within each describe block as in the
third test_spec.rb

I want a notify resource that changes its output based on a
fact named 'os', so first I'm going to define the test that
describes the catalog that I expect

Now that I see that these tests are failing, and they are failing
for the right reason, I'm going to update the code to pass the
test.

End: we can now check if the catalog that we are writing will
compile with the expected resources.

For additional matchers, check out the Readme for the rspec-puppet
Github repository.

## Links

- [rspec-puppet github repository](https://github.com/rodjek/rspec-puppet)
- [puppetlabs_spec_helper github repository](https://github.com/puppetlabs/puppetlabs_spec_helper)
